# Import the arg_handler module that resides in the same directory
from arg_handler import CLIArgs

# Instantiate our class
_args_ = CLIArgs()

# Extract the parsed argument data from the object
parsed = _args_.parsed

# Print it to the terminal
print(parsed)

# Load second module that needs to access argument parser state
import other_module
