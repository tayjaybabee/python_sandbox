from inspy_logger import InspyLogger, LEVELS
from argparse import ArgumentParser

from pypattyrn.creational.singleton import Singleton


class CLIArgs(object,metaclass=Singleton):
    def __init__(self):
        self.parsed = None
        
        parser = ArgumentParser()
        
        verbosity = parser.add_mutually_exclusive_group()
        verbosity.add_argument('-v', '--verbose', 
                               help='Tell the program that you want it to output all the information it has, regardless of granularity.',
                               action='store_true',
                               required=False,
                               default=False)
        verbosity.add_argument('-l', '--log-level',
                               help=f'Specify the level at which the logger will output. Your choices are: {", ".join(LEVELS)}',
                               nargs='?',
                               choices=LEVELS,
                               required=False,
                               default='info')
                              
                               
        self.parsed = parser.parse_args()


if __name__ == '__main__':
    args = CLIArgs()
    parsed_args = args.parsed
    print(parsed_args)

